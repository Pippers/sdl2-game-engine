//
//  load_media.cpp
//  StarRogue
//
//  Created by Philip Varner on 12/11/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#include "global.h"

//
// This loads all our images and fonts
//
bool loadMedia()
{
    /*
     
     button[0 .. 100]
     0 - New Game
     1 - New Game Selected
     2 - Continue
     3 - Continue Selected
     4 - Options
     5 - Options Selected
     6 - Quit
     7 - Quit Selected
     
     images[0 .. 100]
     0 - BG Title Screen
     1 - BG Character select
     
     assets[0 .. 100]
     0 - Captain image STANDING STARING
     1 - Captain image STAND 1 BLINK begin
     2 - Captain image STAND 2 BLINK
     3 - Captain image STAND 3 BLINK end
     4 - Captain image WALK 1 end
     
     music[0..10]
     0 - Title screen theme "Day.ogg"

     font[0 .. 10]
     0 = small verdana
     1 = regular verdana
     2 = regular 8-Bit Wonder

     */
    
    // Turns off "smoothing" when drawing scaled images.
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");
    
    printf("Loading media...\n");
	//Loading success flag
	bool success = true;
    
	std::string tmp_path = path+"images/";
	
    //Load images
    images[0] = loadTexture( tmp_path+"title.png" );
    images[1] = loadTexture( tmp_path+"bg_character_select.png");
    
    //Load assets
    tmp_path = path+"assets/";
    assets[0] = loadTexture( tmp_path+"captain.png");
    assets[1] = loadTexture( tmp_path+"captain_stand_1.png");
    assets[2] = loadTexture( tmp_path+"captain_stand_2.png");
    assets[3] = loadTexture( tmp_path+"captain_stand_3.png");
    assets[4] = loadTexture( tmp_path+"captain_walk_1.png");
    
    //Load buttons
    tmp_path = path+"buttons/";
	button[0] = loadTexture( tmp_path+"button_new_game.png" );
	button[1] = loadTexture( tmp_path+"button_selected_new_game.png" );
	button[2] = loadTexture( tmp_path+"button_continue.png" );
	button[3] = loadTexture( tmp_path+"button_selected_continue.png" );
	button[4] = loadTexture( tmp_path+"button_options.png" );
	button[5] = loadTexture( tmp_path+"button_selected_options.png" );
	button[6] = loadTexture( tmp_path+"button_quit.png" );
	button[7] = loadTexture( tmp_path+"button_selected_quit.png" );
   
    
    //Load fonts
    tmp_path = path+"fonts/";
    std::string loadfont;
    loadfont = tmp_path+main_font;
    font[0]=TTF_OpenFont(loadfont.c_str(), 9);
    font[1]=TTF_OpenFont(loadfont.c_str(), 12);
    loadfont = tmp_path+bit_font;
    font[2]=TTF_OpenFont(loadfont.c_str(), 12);

    //Load music
    tmp_path = path+"music/";
    std::string loadmusic;
    loadmusic = tmp_path + "night.ogg";
    music[0] = Mix_LoadMUS(loadmusic.c_str());
    loadmusic = tmp_path + "day.ogg";
    music[1] = Mix_LoadMUS(loadmusic.c_str());
    loadmusic = tmp_path + "title.ogg";
    music[2] = Mix_LoadMUS(loadmusic.c_str());
    
    return success;
}


//
// This loads the actual image, assigns it to a texture.
//
SDL_Texture* loadTexture( std::string path )
{
	//The final texture
	SDL_Texture* newTexture = NULL;
    
	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
        
		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );
	}
    
	return newTexture;
}

