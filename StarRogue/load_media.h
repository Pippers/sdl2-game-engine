//
//  load_media.h
//  StarRogue
//
//  Created by Philip Varner on 12/11/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#ifndef __StarRogue__load_media__
#define __StarRogue__load_media__

// Main loading loops
bool loadMedia();

// Loads individual image as texture
SDL_Texture* loadTexture( std::string path );

#endif /* defined(__StarRogue__load_media__) */
