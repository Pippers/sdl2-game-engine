//
//  main_menu.cpp
//  StarRogue
//
//  Created by Philip Varner on 12/10/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#include "global.h"

//
// The draw routine for the main menu screen. Shows title screen and buttons.
//
void main_menu(){
    
    if(main_menu_init == 0){main_menu_int();}
    
    // Draw the title screen / background
    DrawImageStretched( images[0], 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    Draw_Stars();
    // Draw the buttons
    DrawImage(button[0], 400, 300);
    DrawImage(button[2], 400, 350);
    DrawImage(button[4], 400, 400);
    DrawImage(button[6], 400, 450);
    mouse_over(mousex, mousey);
}

//
// This will set everything up for the screen
//
void main_menu_int(){
    main_menu_init = 1;
    Mix_PlayMusic(music[0], -1);
    Mix_VolumeMusic(50);
    
    // Create 100 stars.

    for(int i=0;i<300;i++){
        stars = new eye_candy;
        stars->x = rand() % SCREEN_WIDTH;
        stars->y = rand() % SCREEN_HEIGHT;
        stars->vel = rand() % 5+1;
        stars_index.push_back(*stars);
    }
    
}

//
// Draw stars!
//
void Draw_Stars(){
    SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);
    /// Iterate through the vector.
    std::vector<eye_candy>::iterator its;
    for( its = stars_index.begin(); its != stars_index.end(); its++){
        its->x=its->x-its->vel;
        if(its->x<0){its->x=rand() % SCREEN_WIDTH + 500;}
        SDL_RenderDrawPoint(gRenderer, its->x, its->y);
    }

}

//
// This will highlight the button the mouse pointer is hovering over
//
void mouse_over(int mx, int my){
    if( (mx>400) & (mx<590) & (my>300) & (my<325) ){ DrawImage(button[1], 400, 300); }
    if( (mx>400) & (mx<590) & (my>350) & (my<375) ){ DrawImage(button[3], 400, 350); }
    if( (mx>400) & (mx<590) & (my>400) & (my<425) ){ DrawImage(button[5], 400, 400); }
    if( (mx>460) & (mx<545) & (my>450) & (my<475) ){ DrawImage(button[7], 400, 450); }
}

//
// This checks the button clicks of the mouse on the main menu
//
void mousebutton_main_menu(int mx, int my){
    // Main menu buttons are 190x25 in size
    
    //New Game button 400,300 to 590,325
    if( (mx>400) & (mx<590) & (my>300) & (my<325) ){ printf("PUSHED New Game\n"); CURRENT_LOCATION = "NEW_GAME";}
    
    //Continue button 400,350 to 590,375
    if( (mx>400) & (mx<590) & (my>350) & (my<375) ){ printf("PUSHED Continue\n");}
    
    //New Game button 400,400 to 590,425
    if( (mx>400) & (mx<590) & (my>400) & (my<425) ){ printf("PUSHED Options\n");}
    
    //New Game button 460,450 to 545,475
    if( (mx>460) & (mx<545) & (my>450) & (my<475) ){ printf("PUSHED Quit\n");quit = true;}
}