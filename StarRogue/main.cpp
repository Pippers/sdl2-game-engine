//
//  main.cpp
//  StarRogue
//
//  Created by Philip Varner on 12/5/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//


#include "global.h"

// Set all the default game variables.
int mousex;
int mousey;
double angle;
int SCREEN_WIDTH = 1024;
int SCREEN_HEIGHT = 768;
const int SCREEN_FPS = 60;
const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;
std::stringstream timeText;
unsigned int lastTime, currentTime;
bool animate_now = false;
SDL_WindowFlags SCREEN_TYPE = SDL_WINDOW_SHOWN;
std::string CURRENT_LOCATION = "MAIN_MENU";
bool DEBUG_MODE = true;
bool quit = false;
int new_game_init=0;
int main_menu_init=0;
std::string path = "res/";

std::vector<sprite> sprite_index;
sprite *sprites;

std::vector<eye_candy> stars_index;
eye_candy *stars;

/* We're going to be requesting certain things from our audio
 device, so we set them up beforehand */
int audio_rate = 22050;
Uint16 audio_format = AUDIO_S16; /* 16-bit stereo */
int audio_channels = 2;
int audio_buffers = 4096;


// Fonts
std::string main_font = "Verdana.ttf";
std::string bit_font  = "8-BIT WONDER.TTF";

// SDL2 assignments
SDL_Color black={0,0,0,255},white={255,255,255,255},red={255,0,0,255},blue={0,0,255,255},green={0,255,0,255};
SDL_Event e;
SDL_DisplayMode current;
SDL_Window *gwindow;
SDL_Renderer *gRenderer;
SDL_Texture *assets[100];  // Game sprites and objects
SDL_Texture *button[100];  // Game buttons
SDL_Texture *images[100];  // Game images (Title screen, etc)
SDL_Texture *text;
TTF_Font *font[10];
Mix_Music *music[10];

//The application time based timer
class LTimer
{
private:
    //The clock time when the timer started
    Uint32 mStartTicks;
    
    //The ticks stored when the timer was paused
    Uint32 mPausedTicks;
    
    //The timer status
    bool mPaused;
    bool mStarted;
    
public:
    //Initializes variables
    LTimer();
    
    //The various clock actions
    void start();
    void stop();
    void pause();
    void unpause();
    
    //Gets the timer's time
    Uint32 getTicks();
    
    //Checks the status of the timer
    bool isStarted();
    bool isPaused();
};

LTimer::LTimer()
{
    //Initialize the variables
    mStartTicks = 0;
    mPausedTicks = 0;
    
    mPaused = false;
    mStarted = false;
}

void LTimer::start()
{
    //Start the timer
    mStarted = true;
    
    //Unpause the timer
    mPaused = false;
    
    //Get the current clock time
    mStartTicks = SDL_GetTicks();
	mPausedTicks = 0;
}

void LTimer::stop()
{
    //Stop the timer
    mStarted = false;
    
    //Unpause the timer
    mPaused = false;
    
	//Clear tick variables
	mStartTicks = 0;
	mPausedTicks = 0;
}

void LTimer::pause()
{
    //If the timer is running and isn't already paused
    if( mStarted && !mPaused )
    {
        //Pause the timer
        mPaused = true;
        
        //Calculate the paused ticks
        mPausedTicks = SDL_GetTicks() - mStartTicks;
		mStartTicks = 0;
    }
}

void LTimer::unpause()
{
    //If the timer is running and paused
    if( mStarted && mPaused )
    {
        //Unpause the timer
        mPaused = false;
        
        //Reset the starting ticks
        mStartTicks = SDL_GetTicks() - mPausedTicks;
        
        //Reset the paused ticks
        mPausedTicks = 0;
    }
}

Uint32 LTimer::getTicks()
{
	//The actual timer time
	Uint32 time = 0;
    
    //If the timer is running
    if( mStarted )
    {
        //If the timer is paused
        if( mPaused )
        {
            //Return the number of ticks when the timer was paused
            time = mPausedTicks;
        }
        else
        {
            //Return the current time minus the start time
            time = SDL_GetTicks() - mStartTicks;
        }
    }
    
    return time;
}

bool LTimer::isStarted()
{
	//Timer is running and paused or unpaused
    return mStarted;
}

bool LTimer::isPaused()
{
	//Timer is running and paused
    return mPaused && mStarted;
}

//Starts up SDL and creates window
bool init();

//Frees media and shuts down SDL
void close();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//Draw image on the screen at IMAGE X Y
void DrawImage(SDL_Texture *tex, int xa, int ya);

//Draw image with more options. ROTATION ANGLE and FLIP
void DrawImageEx(SDL_Texture *tex, int xa, int ya, double degree, SDL_RendererFlip flip);

//Draw an image with stretched dimensions
void DrawImageStretched(SDL_Texture *tex, int location_x, int location_y, int stretched_x, int stretched_y);

//Write text onto the screen
void Print(std::string text, int xa, int ya, int font_selection, SDL_Color font_color);

//Debug stuff goes in here
void Debug();

bool init()
{
    srand (time_t(NULL));
    SDL_DisplayMode current;
// OSX This makes loading resources and such local to the app.
#ifdef __APPLE__
    printf("Setting APPLE defines.\n");
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(mainBundle);
    char path[PATH_MAX];
    if (!CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, PATH_MAX)) // Error: expected unqualified-id before 'if'
    {
        // error!
    }
    CFRelease(resourcesURL); // error: expected constructor, destructor or type conversion before '(' token
    
    chdir(path); // error: expected constructor, destructor or type conversion before '(' token
    std::cout << "Current Path: " << path << std::endl; // error: expected constructor, destructor or type conversion before '<<' token
#endif
    
	//Initialization flag
	bool success = true;
    
	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}
        
		//Create window
		gWindow = SDL_CreateWindow( "Star Rogue - Adventures in Space-Time", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TYPE);
        if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( gRenderer, 0x0, 0x0, 0x0, 0xFF );
                
				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
                //Initialize the AUDIO
                if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers))
                {
                    printf("Unable to open audio!\n");
                    success = false;
                }
                //SDL_RenderSetLogicalSize(gRenderer, 2048, 1536);
                //This will get the display mode settings
                SDL_GetCurrentDisplayMode(0, &current);
                printf("Current display mode is %dx%dpx\n",current.w,current.h);
			}
		}
	}
    for(int i=0;i<=10;i++){font[i]=NULL;}
    for(int i=0;i<=10;i++){music[i]=NULL;}
    TTF_Init();
    printf("Initilization success.\n");
	return success;
}


void close()
{
    printf("Shutting down.\n");
	//Free loaded images
    
    for (int i=0; i<=100; i++) {
        SDL_DestroyTexture( button[i] );
        button[i] = NULL;
        SDL_DestroyTexture( assets[i]);
        assets[i] = NULL;
        SDL_DestroyTexture( images[i] );
        images[i] = NULL;
    }
    
    // Free fonts and music
    /* TODO fix this
    for (int i=0; i<=10; i++) {
        Mix_FreeMusic(music[i]);
        TTF_CloseFont(font[i]);
    }
    */
    
    SDL_DestroyTexture( text );
	text = NULL;
    
	//Destroy window
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;
    
	//Quit SDL subsystems
    TTF_Quit();
	IMG_Quit();
    Mix_CloseAudio();
	SDL_Quit();
}

//
// Debug mode goes here
//
void Debug(){
 
    // Mouse coordinates
    Print("Mouse X: "+std::to_string(mousex),10,SCREEN_HEIGHT-15,0,white);
    Print("Mouse Y: "+std::to_string(mousey),90,SCREEN_HEIGHT-15,0,white);
    Print("FPS: "+timeText.str(), 170, SCREEN_HEIGHT-15, 0, white);
    Print("Sprites: "+std::to_string(sprite_index.size()), 250, SCREEN_HEIGHT-15, 0, white);
}


//
// Draw image to the screen
//
void DrawImage(SDL_Texture *tex, int xa, int ya){
    
    SDL_Rect srcrect;   // Source RECT to get base image dimensions.
    SDL_Rect dstrect;   // Destination RECT to set image dimensions to the screen.
    
    srcrect.x = 0;
    srcrect.y = 0;
    
    dstrect.x = xa;
    dstrect.y = ya;
    
    // Look at the image, set Rects obj WIDTH and HEIGHT.
    SDL_QueryTexture(tex, NULL, NULL, &srcrect.w, &srcrect.h);
    SDL_QueryTexture(tex, NULL, NULL, &dstrect.w, &dstrect.h);
    SDL_RenderCopy( gRenderer, tex, &srcrect, &dstrect );
}

//
// Draw image with options ROTATION DEGREE, and FLIP
//
//  degree = 0.0;
//  flip = SDL_FLIP_NONE;   SDL_FLIP_HORIZONTAL
//
void DrawImageEx(SDL_Texture *tex, int xa, int ya, double degree, SDL_RendererFlip flip){
    SDL_Rect srcrect;   // Source RECT to get base image dimensions.
    SDL_Rect dstrect;   // Destination RECT to set image dimensions to the screen.
    
    srcrect.x = 0;
    srcrect.y = 0;
    
    dstrect.x = xa;
    dstrect.y = ya;
    
    // Look at the image, set Rects obj WIDTH and HEIGHT.
    SDL_QueryTexture(tex, NULL, NULL, &srcrect.w, &srcrect.h);
    SDL_QueryTexture(tex, NULL, NULL, &dstrect.w, &dstrect.h);
    SDL_RenderCopyEx( gRenderer,tex, &srcrect, &dstrect, degree, NULL, flip );
}

//
// Draw image to the screen to a certain size
//
void DrawImageStretched(SDL_Texture *tex, int location_x, int location_y, int stretched_x, int stretched_y){
    
    SDL_Rect srcrect;   // Source RECT to get base image dimensions.
    SDL_Rect dstrect;   // Destination RECT to set image dimensions to the screen.
    
    srcrect.x = 0;
    srcrect.y = 0;
    
    dstrect.x = location_x;
    dstrect.y = location_y;
    dstrect.w = stretched_x;
    dstrect.h = stretched_y;
    
    // Look at the image, set Rects obj WIDTH and HEIGHT.
    SDL_QueryTexture(tex, NULL, NULL, &srcrect.w, &srcrect.h);
    SDL_RenderCopy( gRenderer, tex, &srcrect, &dstrect );
}

//
//Write text onto the screen
//
void Print(std::string text, int xa, int ya, int font_selection, SDL_Color font_color){
    
    int fontw, fonth;
    TTF_SizeText(font[font_selection], text.c_str(), &fontw, &fonth);
    SDL_Surface *mytext=TTF_RenderText_Solid(font[font_selection],text.c_str(),font_color);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(gRenderer, mytext);
    DrawImage(texture,xa,ya);
    SDL_DestroyTexture( texture );
    SDL_FreeSurface( mytext );
}

int main( int argc, char* args[] )
{
	//Start up SDL and create window
	if( !init() )
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{
            
            //The frames per second timer
			LTimer fpsTimer;
            
			//The frames per second cap timer
			LTimer capTimer;
            
            //Start counting frames per second
			int countedFrames = 0;
			fpsTimer.start();
            
            
			//Event handler
			SDL_Event e;
            printf("Entering game loop.\n");
			//While application is running.
			while( !quit )
			{
                // Print a report once per second
                currentTime = SDL_GetTicks();
                
                // One second ticks
                if (currentTime > lastTime + 1000) {
                    lastTime = currentTime;
                }

                //Start cap timer
				capTimer.start();
                
                //Calculate and correct fps
				float avgFPS = countedFrames / ( fpsTimer.getTicks() / 1000.f );
				if( avgFPS > 2000000 )
				{
					avgFPS = 0;
				}
                //Set FPS to string
				timeText.str( "" );
				timeText << avgFPS;
                
                //Clear screen
                SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 255);
                SDL_RenderClear( gRenderer );
                
                // Locational loops for drawing, etc
                if(CURRENT_LOCATION=="MAIN_MENU") {main_menu();}
                if(CURRENT_LOCATION=="NEW_GAME")  {new_game_character();}
                
                //Handle events on queue.
                while( SDL_PollEvent( &e ) != 0 )
                {
                    //User requests quit
                    if( e.type == SDL_QUIT )
                    {
                        quit = true;
                    }
                    if( e.type == SDL_MOUSEBUTTONDOWN )
                    {
                        SDL_GetMouseState(&mousex, &mousey);
                        if(CURRENT_LOCATION=="NEW_GAME") {mousebutton_generate_character(mousex,mousey);}
                        if(CURRENT_LOCATION=="MAIN_MENU"){mousebutton_main_menu(mousex,mousey);}
                    }
                    if( e.type == SDL_KEYDOWN)
                    {
                        switch( e.key.keysym.sym )
                        {
                            case SDLK_a:
                                angle -= 1;
                                break;
                                
                            case SDLK_d:
                                angle += 1;
                                break;
                                
                            case SDLK_q:
                                quit = true;
                                //   flipType = SDL_FLIP_HORIZONTAL;
                                break;
                                
                            case SDLK_w:
                                //    flipType = SDL_FLIP_NONE;
                                break;
                                
                            case SDLK_e:
                                //    flipType = SDL_FLIP_VERTICAL;
                                break;
                        }
                    }
                    if( e.type == SDL_MOUSEMOTION )
                    {
                        SDL_GetMouseState(&mousex, &mousey);
                    }
                    
                }
               
                if(DEBUG_MODE){Debug();}
                
                //Update screen
                SDL_RenderPresent( gRenderer );
                ++countedFrames;
                
				//If frame finished early
				int frameTicks = capTimer.getTicks();
				if( frameTicks < SCREEN_TICK_PER_FRAME )
				{
					//Wait remaining time
					SDL_Delay( SCREEN_TICK_PER_FRAME - frameTicks );
				}
			}
		}
	}
    
	//Free resources and close SDL
	close();
    
	return 0;
    
}

