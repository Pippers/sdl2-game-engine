//
//  global.h
//  StarRogue
//
//  Created by Philip Varner on 12/5/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#ifndef GLOBAL
#define GLOBAL

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif


// System includes
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <vector>
#include <math.h>
#include <stdlib.h>

// SDL2 includes
#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_mixer/SDL_mixer.h>

// Game includes
#include "main_menu.h"
#include "load_media.h"
#include "generate_character.h"
#include "sprites.h"

// Global variables for all
extern std::stringstream timeText;
extern int mousex;
extern int mousey;
extern double angle;
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern SDL_WindowFlags SCREEN_TYPE;
extern std::string CURRENT_LOCATION;
extern bool DEBUG_MODE;
extern bool quit;
extern unsigned int lastTime, currentTime;
extern bool animate_now;
extern std::string path;
extern std::string main_font;
extern std::string bit_font;

// Identifiers
extern int main_menu_init;
extern int new_game_init;

// SDL2 definitions for all
extern SDL_Event e;
extern SDL_DisplayMode current;
extern SDL_Window *gwindow;
extern SDL_Renderer *gRenderer;
extern SDL_Texture *assets[100];  // Game sprites and objects
extern SDL_Texture *button[100];  // Game buttons
extern SDL_Texture *images[100];  // Game images (Title screen, etc)
extern SDL_Texture *text;
extern TTF_Font *font[10];
extern SDL_Color black,white,red,blue,green;
extern Mix_Music *music[10];

extern sprite *sprites;                     // Game sprites
extern std::vector<sprite> sprite_index;    // Index all the sprites created
extern std::vector<sprite>::iterator it;    // Iterator for the index.

extern eye_candy *stars;
extern std::vector<eye_candy> stars_index;
extern std::vector<eye_candy>::iterator its;

// Shared routines available to all
extern void DrawImage(SDL_Texture *tex, int xa, int ya);
extern void DrawImageEx(SDL_Texture *tex, int xa, int ya, double degree, SDL_RendererFlip flip);
extern void DrawImageStretched(SDL_Texture *tex, int location_x, int location_y, int stretched_x, int stretched_y);
extern void Print(std::string text, int xa, int ya, int font_selection, SDL_Color font_color);
extern void close();

#endif
