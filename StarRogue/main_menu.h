//
//  main_menu.h
//  StarRogue
//
//  Created by Philip Varner on 12/10/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#ifndef __StarRogue__main_menu__
#define __StarRogue__main_menu__

class eye_candy{
public:
    int x;
    int y;
    int vel;
};

void main_menu();
void mouse_over(int mx, int my);
void mousebutton_main_menu(int mx, int my);
void main_menu_int();
void Draw_Stars();

#endif /* defined(__StarRogue__main_menu__) */
