//
//  sprites.h
//  StarRogue
//
//  Created by Philip Varner on 12/13/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#ifndef __StarRogue__sprites__
#define __StarRogue__sprites__



class sprite{
    
public:
    
    int x;//
    int y;//
    int created = 0;
    int anim = 0, frame = 0;
    unsigned int spriteTime = 0;
    int current_frame = 0;
    
    //  #0
    // Standing animation
    std::vector <int> stand_anim = {0,1,2,3};
    std::vector <int> stand_anim_delay = {5000,20,20,20};
    
    // #1
    // Walking animation
    std::vector <int> walk_anim = {0,4};
    std::vector <int> walk_anim_delay = {350,350};
    
    // #2
    // 2 step animation
    std::vector <int> step2_anim = {0,4};
    std::vector <int> step2_anim_delay = {350,350};
    
    // #3
    // 3 step animation
    std::vector <int> step3_anim = {0,4,4};
    std::vector <int> step3_anim_delay = {350,350,350};
    
    // #4
    // 4 step animation
    std::vector <int> step4_anim = {0,4,4,4};
    std::vector <int> step4_anim_delay = {350,350,350,350};
    
public:
    void draw_frame();
    void set_options(int startx, int starty, int startanim);
    
};

//
// This looks for any sprites that have been created and loops through them adjusting frames as needed.
//
void UpdateSprites();


#endif /* defined(__StarRogue__sprites__) */
