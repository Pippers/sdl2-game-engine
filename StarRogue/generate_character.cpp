//
//  generate_character.cpp
//  StarRogue
//
//  Created by Philip Varner on 12/11/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#include "global.h"

//
// Loop for the character generation screen
//
void new_game_character(){
    
    // Set up our initial sprites
    if(new_game_init == 0){new_game_int();}
    
    // Draw everything
    DrawImageStretched( images[1], 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    Print("Create a new character                      Click anywhere", 10, 10, 2, white);
    UpdateSprites();
    
}

//
// This will set everything up for the screen
//
void new_game_int(){
    new_game_init = 1;
    
    Mix_FadeOutChannel(-1, 500);
    Mix_PlayMusic(music[1], -1);
    // Add our first sprite!
    sprites = new sprite;
    sprites->set_options(0, 50, 0);
    
}


//
// This checks mouse clicks on the character generation screen
//
void mousebutton_generate_character(int x, int y){
    
    sprites = new sprite;
    sprites->set_options(x-32, y-37, 0);

    
}