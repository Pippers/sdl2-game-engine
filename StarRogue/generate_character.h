//
//  generate_character.h
//  StarRogue
//
//  Created by Philip Varner on 12/11/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#ifndef __StarRogue__generate_character__
#define __StarRogue__generate_character__

void new_game_character();
void new_game_int();
void mousebutton_generate_character(int x, int y);

#endif /* defined(__StarRogue__generate_character__) */
