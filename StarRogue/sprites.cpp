//
//  sprites.cpp
//  StarRogue
//
//  Created by Philip Varner on 12/13/13.
//  Copyright (c) 2013 PV-Studios. All rights reserved.
//

#include "global.h"

void sprite::draw_frame(){
    
    // Standing animation
    if (anim == 0)
    {
        if (currentTime > spriteTime + stand_anim_delay[current_frame]){current_frame++;spriteTime = currentTime;}
        
        if (current_frame == stand_anim.size())
        {
            current_frame = 0;
            stand_anim_delay[0] = rand() % 7000;
        }
        frame = stand_anim[current_frame];
    }
    
    // Walking animation
    if (anim == 1)
    {
        if (currentTime > spriteTime + walk_anim_delay[current_frame]){current_frame++;spriteTime = currentTime;}
        
        if (current_frame == walk_anim.size())
        {
            current_frame = 0;
        }
        frame = walk_anim[current_frame];
    }
    
    // 2 step animation
    if (anim == 2)
    {
        if (currentTime > spriteTime + step2_anim_delay[current_frame]){current_frame++;spriteTime = currentTime;}
        
        if (current_frame == step2_anim.size())
        {
            current_frame = 0;
        }
        frame = step2_anim[current_frame];
    }
    // 3 step animation
    if (anim == 3)
    {
        if (currentTime > spriteTime + step3_anim_delay[current_frame]){current_frame++;spriteTime = currentTime;}
        
        if (current_frame == step3_anim.size())
        {
            current_frame = 0;
        }
        frame = step3_anim[current_frame];
    }
    // 4 step animation
    if (anim == 4)
    {
        if (currentTime > spriteTime + step4_anim_delay[current_frame]){current_frame++;spriteTime = currentTime;}
        
        if (current_frame == step4_anim.size())
        {
            current_frame = 0;
        }
        frame = step4_anim[current_frame];
    }
    
    
    DrawImage(assets[frame], x, y);
}

void sprite::set_options(int startx, int starty, int startanim){
    x = startx;
    y = starty;
    anim = startanim;
    created = 1;
    sprite_index.push_back(*sprites);
    
}

void UpdateSprites(){
    
	/// Iterate through the vector.
	std::vector<sprite>::iterator it;
    for( it= sprite_index.begin(); it != sprite_index.end(); it++){
        it->draw_frame();
    }
    
}